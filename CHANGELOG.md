# Change LOG

## v3.2.0
1. Update to Spring Boot 3.4.2
2. Update to Spring Cloud 2024.0.0
3. Update to Spring Cloud GCP 6.0.0
4. Update to JVM 21
5. Update dependencies

## v3.1.0
1. Update to Spring Boot 3.3.7
2. Update to Spring Cloud 2023.0.4
3. Update to Spring Cloud GCP 5.9.0

## v3.0.0
1. Change to Spring Boot 3.2.5

## v2.0.0
1. Repackaging

## v1.0.1
1. Add setting retrieval on ThreadPoolConfig.

## v1.0.0
1. Initial version.
2. Booster-commons v1.0.1
3. Booster-http-client v1.0.2
4. Booster-web v1.0.0
5. Spring Boot 2.7.6
6. Spring Cloud 2021.0.5
