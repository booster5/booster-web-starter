# Booster Web Starter

## Getting started

### Project Setup

#### Maven

pom.xml
```xml
<dependency>
  <groupId>io.gitlab.booster</groupId>
  <artifactId>booster-web-starter</artifactId>
  <version>VERSION_TO_USE</version>
</dependency>
```
if you haven't yet set up your repository:
```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/61046646/-/packages/maven</url>
  </repository>
</repositories>
```

#### Gradle Groovy

```groovy
implementation 'io.gitlab.booster:booster-web-starter:VERSION_TO_USE'
```
setup maven repository:
```groovy
maven {
  url 'https://gitlab.com/api/v4/groups/61046646/-/packages/maven'
}
```

#### Gradle Kotlin

```kotlin
implementation("io.gitlab.booster:booster-web-starter:VERSION_TO_USE")
```
setup maven repository:
```kotlin
maven("https://gitlab.com/api/v4/groups/61046646/-/packages/maven")
```

## Usage 

This starter library includes:
1. booster-commons,
2. booster-task,
3. booster-http-client, and
4. booster-web

Spring auto-configuration is enabled and all features are ready to use if you include 
this starter package to your spring project pom.

### HTTP Client 

To create HTTP clients, see [booster-http-client documentation](https://gitlab.com/booster5/booster-http-client).

### Asynchronous Task Execution.

To create tasks that can be executed asynchronously either in parallel or in sequence, see 
[booster-task documentation](https://gitlab.com/booster5/booster-task)

### Restful Endpoints 

To create Restful endpoints, see [booster-web documentation](https://gitlab.com/booster5/booster-web).
