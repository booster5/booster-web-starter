package io.gitlab.booster.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.gitlab.booster.commons.circuit.breaker.CircuitBreakerConfig;
import io.gitlab.booster.commons.metrics.MetricsRegistry;
import io.gitlab.booster.commons.retry.RetryConfig;
import io.gitlab.booster.config.thread.ThreadPoolConfig;
import io.gitlab.booster.factories.HttpClientFactory;
import io.gitlab.booster.factories.TaskFactory;
import io.gitlab.booster.http.client.config.BoosterObservationConvention;
import io.gitlab.booster.http.client.config.HttpClientConnectionConfig;
import io.micrometer.core.instrument.MeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.ClientRequestObservationConvention;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Auto configuration for Booster Web, HTTP client and tasks.
 */
@Configuration
public class BoosterConfig {

    /**
     * Service tag
     */
    public static final String NAME = "service";

    /**
     * Environment tag
     */
    public static final String PROFILE = "environment";

    private static final Logger log = LoggerFactory.getLogger(BoosterConfig.class);

    private final ConfigurableApplicationContext configurableApplicationContext;

    /**
     * Default constructor
     * @param configurableApplicationContext {@link ConfigurableApplicationContext} used to register thread pools as beans.
     */
    public BoosterConfig(@Autowired ConfigurableApplicationContext configurableApplicationContext) {
        this.configurableApplicationContext = configurableApplicationContext;
    }

    /**
     * Creates metrics recorder
     *
     * @param meterRegistry internal {@link MeterRegistry} to be used to record metrics
     * @return {@link MetricsRegistry}
     */
    @Bean
    public MetricsRegistry metricsRegistry(
            @Autowired(required = false) MeterRegistry meterRegistry
    ) {
        return new MetricsRegistry(meterRegistry);
    }

    /**
     * Creates a {@link ThreadPoolConfig} bean
     * @param registry {@link MetricsRegistry} to record metrics
     * @return {@link ThreadPoolConfig} bean
     */
    @Bean
    @ConfigurationProperties(prefix = "booster.tasks.threads")
    public ThreadPoolConfig threadPoolConfig(
            @Autowired MetricsRegistry registry
    ) {
        return new ThreadPoolConfig(
                this.configurableApplicationContext,
                registry
        );
    }

    /**
     * Creates a {@link CircuitBreakerConfig} bean
     * @return {@link CircuitBreakerConfig} bean
     */
    @Bean
    @ConfigurationProperties(prefix = "booster.tasks.circuit-breakers")
    public CircuitBreakerConfig circuitBreakerConfig() {
        return new CircuitBreakerConfig();
    }

    /**
     * Creates a {@link HttpClientConnectionConfig} bean
     * @return {@link HttpClientConnectionConfig} bean
     */
    @Bean
    @ConfigurationProperties(prefix = "booster.http.clients.connections")
    public HttpClientConnectionConfig httpClientConnectionConfig() {
        return new HttpClientConnectionConfig(this.configurableApplicationContext);
    }

    /**
     * Creates a {@link ClientRequestObservationConvention} bean
     * @return {@link ClientRequestObservationConvention} bean
     */
    @Bean
    public ClientRequestObservationConvention clientRequestObservationConvention() {
        return new BoosterObservationConvention();
    }

    /**
     * Creates a {@link RetryConfig} bean
     * @return {@link RetryConfig} bean
     */
    @Bean
    @ConfigurationProperties(prefix = "booster.tasks.retries")
    public RetryConfig retryConfig() {
        return new RetryConfig();
    }

    /**
     * Creates {@link HttpClientFactory} bean
     * @param config {@link HttpClientConnectionConfig}
     * @param webClientBuilder {@link WebClient.Builder} for trace injection
     * @param mapper {@link ObjectMapper} for serialization and deserialization of requests/responses
     * @return {@link HttpClientFactory} bean
     */
    @Bean
    public HttpClientFactory httpClientFactory(
            @Autowired HttpClientConnectionConfig config,
            @Autowired WebClient.Builder webClientBuilder,
            @Autowired(required = false) ObjectMapper mapper
    ) {
        return new HttpClientFactory(config, webClientBuilder, mapper);
    }

    /**
     * Creates {@link TaskFactory} bean
     * @param threadPoolConfig {@link ThreadPoolConfig} for {@link io.gitlab.booster.task.Task}
     * @param retryConfig {@link RetryConfig} for {@link io.gitlab.booster.task.Task}
     * @param circuitBreakerConfig {@link CircuitBreakerConfig} for {@link io.gitlab.booster.task.Task}
     * @param httpClientFactory {@link HttpClientFactory} to create HTTP {@link io.gitlab.booster.task.Task}
     * @param registry {@link MetricsRegistry} to record metrics
     * @return {@link TaskFactory} bean
     */
    @Bean
    public TaskFactory TaskFactory(
            @Autowired ThreadPoolConfig threadPoolConfig,
            @Autowired RetryConfig retryConfig,
            @Autowired CircuitBreakerConfig circuitBreakerConfig,
            @Autowired HttpClientFactory httpClientFactory,
            @Autowired MetricsRegistry registry
    ) {
        return new TaskFactory(
                threadPoolConfig,
                retryConfig,
                circuitBreakerConfig,
                httpClientFactory,
                registry
        );
    }

    /**
     * Creates {@link MeterRegistryCustomizer} bean to inject common tags on metrics
     * @param serviceName name of service
     * @param activeProfile name of environment
     * @return {@link MeterRegistryCustomizer} bean
     */
    @Bean
    public MeterRegistryCustomizer<MeterRegistry> metricsCommonTags(
            @Value("${spring.application.name:default}") String serviceName,
            @Value("${spring.profiles.active:default}") String activeProfile
    ) {
        return registry -> registry.config().commonTags(
                NAME, serviceName,
                PROFILE, activeProfile
        );
    }
}
